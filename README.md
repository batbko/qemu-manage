Qemu Manage System
===========

## Features
 * Add guest
 * Delete guest
 * Clone guest
 * Show guest status
 * Start/stop guest
 * Connect to guest via vnc
 * Show/Edit guest hardware parametrs
 * USB support
 * Show OpenvSwitch map (TODO)

## Videos
[![Alt Add VM example](http://img.youtube.com/vi/jOtCY--LEN8/1.jpg)](http://www.youtube.com/watch?v=jOtCY--LEN8)

## Environment Requirements
 * Linux host
 * app-emulation/qemu
 * net-misc/openvswitch [optional]
